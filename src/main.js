// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import "./polyfill";
// import cssVars from 'css-vars-ponyfill'
import Vue from "vue";
import BootstrapVue from "bootstrap-vue";
import NProgress from 'vue-nprogress'
import App from "./App";
import router from "./router/index";
import store from "./store/";
import VeeValidate from "vee-validate";
import CKEditor from "@ckeditor/ckeditor5-vue";
const VueUploadComponent = require("vue-upload-component");
Vue.component("file-upload", VueUploadComponent);
import Toaster from "v-toaster";
import "vue-select/dist/vue-select.css";
import VueFormGenerator from 'vue-form-generator'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.prototype.$imageUrl = 'http://api.caceglobal.org/api/files/view/';
Vue.use(NProgress);
const nprogress = new NProgress({ parent: '.nprogress-container' });
Vue.use(VueFormGenerator)

// You need a specific loader for CSS files like https://github.com/webpack/css-loader
//import "v-toaster/dist/v-toaster.css";

// optional set default imeout, the default is 10000 (10 seconds).
Vue.use(Toaster, { timeout: 5000 });

Vue.use(VeeValidate);
Vue.use(CKEditor);
Vue.use(BootstrapVue);

/* eslint-disable no-new */
new Vue({
  el: "#app",
  router,
  store,
  components: {
    nprogress,
    App
  },
  template: "<App/>"
});
