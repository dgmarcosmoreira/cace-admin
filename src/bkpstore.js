import Vue from "vue";
import Vuex from "vuex";
import userService from "./services/user";
import coursesService from "./services/course";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    users: [],
    courses: [],
    course: [],
    loading: false
  },
  mutations: {
    set_users(state, users) {
      state.users = users;
    },
    set_courses(state, courses) {
      state.courses = courses;
    },
    set_course(state, course) {
      state.course = course;
    },
    set_loading(state, loading) {
      state.loading = loading;
    }
  },
  actions: {
    async getUsers({ commit }, data) {
      try {
        const users = await userService.getUser();
        commit("set_users", users);
      } catch (error) {
        alert("error");
      }
    },
    async getCourse({ commit }, courseId) {
      try {
        const course = await coursesService.getCourse(courseId);
        commit("set_course", course);
      } catch (error) {
        alert("error");
      }
    },
    async getCourses({ commit }, count) {
      try {
        const courses = await coursesService.getCourses();
        commit("set_courses", courses);
      } catch (error) {
        alert("error");
      }
    },
    async editCourse({ commit }, data) {
      try {
        await coursesService.editCourse();
      } catch (error) {
        alert("error");
      }
    }
  }
});
