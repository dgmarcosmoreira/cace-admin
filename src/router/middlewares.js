export const isLogged = (to, from, next) => {
  const isLogged = localStorage.getItem('access_token') !== null;
  if (!isLogged) {
    next({ path: '/' });
  } else {
    next();
  }
};

export const requiereAuth = (to, from, next) => {
  const requiereAuth = to.matched.some(record => record.meta.requiereAuth);
  const logged = localStorage.getItem('access_token') !== null;
  if (
    (!requiereAuth && logged && to.path === '/pages/login') ||
    (!requiereAuth && logged && to.path === '/pages/register')
  ) {
      next('/');
  } else {
    next();
  }
};


export const cargarTeatroRedirect = async (to, from, next) => {
  await store.dispatch('theatre/getProfile');

  const hasRooms = store.state.theatre.profile.rooms.length > 0;
  if (hasRooms) {
    next({ path: '/sala/cerca' });
  } else {
    next();
  }
};
