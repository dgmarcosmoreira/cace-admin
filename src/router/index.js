import Vue from "vue";
import Router from "vue-router";

// import {requiereAuth, isLogged} from './middlewares';

// Containers
const DefaultContainer = () => import("@/containers/DefaultContainer");

// Views
const Dashboard = () => import("@/views/Dashboard");

// Cace views
const Courses = () => import("@/views/courses/Courses");
const AddCourse = () => import("@/views/courses/AddCourse");
const EditCourse = () => import("@/views/courses/EditCourse");

const AddClass = () => import("@/views/classes/AddClass");

const Places = () => import("@/views/places/Places");
const Place = () => import("@/views/places/Place");

const Teachers = () => import("@/views/teacher/Teachers");
const Teacher = () => import("@/views/teacher/Teacher");

const Pages = () => import("@/views/pages/Pages");
const Page = () => import("@/views/pages/Page");

const Categories = () => import("@/views/categories/Categories");
const Category = () => import("@/views/categories/Category");

// Views - Pages
const Page404 = () => import("@/views/pages/Page404");
const Page500 = () => import("@/views/pages/Page500");
const Login = () => import("@/views/pages/Login");
const Register = () => import("@/views/pages/Register");
const PassRecover = () => import("@/views/pages/PassRecover");

// Users
const Users = () => import("@/views/users/Users");
const User = () => import("@/views/users/User");

const Orders = () => import("@/views/orders/Orders");
const Inscriptions = () => import("@/views/inscriptions/Inscriptions");

Vue.use(Router);
let router = new Router({
    mode: "hash", // https://router.vuejs.org/api/#mode
    linkActiveClass: "open active",
    scrollBehavior: () => ({y: 0}),
    routes: [
        {
            path: "/",
            redirect: "/dashboard",
            name: "Home",
            component: DefaultContainer,
            children: [
                {
                    path: "dashboard",
                    name: "Dashboard",
                    component: Dashboard,
                    meta: {
                        requiresEditor: true,
                    }
                },
                {
                    path: "courses",
                    name: "Courses",
                    component: Courses,
                    meta: {
                        requiresEditor: true,
                    }
                },
                {
                    path: "course/add",
                    name: "Course Add",
                    component: AddCourse,
                    meta: {
                        requiresEditor: true,
                    }
                },
                {
                    path: "course/:id",
                    name: "Course Edit",
                    component: EditCourse,
                    meta: {
                        requiresEditor: true,
                    }
                },
                {
                    path: "course/:courseId/class/add/unit/:unitId",
                    name: "Class Add",
                    component: AddClass,
                    meta: {
                        requiresEditor: true,
                    }
                },
                {
                    path: "course/:courseId/class/:id/unit/:unitId",
                    name: "Class Edit",
                    component: AddClass,
                    meta: {
                        requiresEditor: true,
                    }
                },
                {
                    path: "places",
                    name: "Places",
                    component: Places,
                    meta: {
                        requiresEditor: true,
                    }
                },
                {
                    path: "place/add",
                    name: "Place Add",
                    component: Place,
                    meta: {
                        requiresEditor: true,
                    }
                },
                {
                    path: "place/:id",
                    name: "Place Edit",
                    component: Place,
                    meta: {
                        requiresEditor: true,
                    }
                },
                {
                    path: "users",
                    name: "Users",
                    component: Users,
                    meta: {
                        requiresLegal: true,
                    }
                },
                {
                    path: "users/add",
                    name: "User Add",
                    component: User,
                    meta: {
                        requiresLegal: true,
                    }
                },
                {
                    path: "users/:id",
                    name: "User Edit",
                    component: User,
                    meta: {
                        requiresLegal: true,
                    }
                },
                {
                    path: "teachers",
                    name: "Teachers",
                    component: Teachers,
                    meta: {
                        requiresEditor: true,
                    }
                },
                {
                    path: "teacher/:id",
                    name: "Teacher Edit",
                    component: Teacher,
                    meta: {
                        requiresEditor: true,
                    }
                },
                {
                    path: "pages",
                    name: "pages",
                    component: Pages,
                    meta: {
                        requiresAdmin: true,
                    }
                },
                {
                    path: "page/add",
                    name: "Page Add",
                    component: Page,
                    meta: {
                        requiresAdmin: true,
                    }
                },
                {
                    path: "page/:id",
                    name: "Page Edit",
                    component: Page,
                    meta: {
                        requiresAdmin: true,
                    }
                },
                {
                    path: "categories",
                    name: "categories",
                    component: Categories,
                    meta: {
                        requiresEditor: true,
                    }
                },
                {
                    path: "category/add",
                    name: "Category Add",
                    component: Category,
                    meta: {
                        requiresEditor: true,
                    }
                },
                {
                    path: "category/:id",
                    name: "Category Edit",
                    component: Category,
                    meta: {
                        requiresEditor: true,
                    }
                },
                {
                    path: "orders",
                    name: "pedidos",
                    component: Orders,
                    meta: {
                        requiresEditor: true,
                    }
                },
                {
                    path: "inscriptions",
                    name: "inscriptions",
                    component: Inscriptions,
                    meta: {
                        requiresEditor: true,
                    }
                },
            ]
        },
        {
            path: "/pages",
            redirect: "/pages/404",
            name: "Pages",
            component: {
                render(c) {
                    return c("router-view");
                }
            },
            children: [
                {
                    path: "404",
                    name: "Page404",
                    component: Page404,
                    meta: {
                        guest: true
                    }
                },
                {
                    path: "500",
                    name: "Page500",
                    component: Page500,
                    meta: {
                        guest: true
                    }
                },
                {
                    path: "login",
                    name: "Login",
                    component: Login,
                    meta: {
                        guest: true
                    }
                },
                {
                    path: "register",
                    name: "Register",
                    component: Register,
                    meta: {
                        guest: true
                    }
                },
                {
                    path: "recover-pass",
                    name: "PassRecover",
                    component: PassRecover,
                    meta: {
                        guest: true
                    }
                }
            ]
        }
    ]
});

/* router.beforeEach((to, from, next) => {
    // no esta logueado
    user.getMyProfile().then(function (response) {
        if (
            to.name === 'Login' ||
            to.name === 'Register' ||
            to.name === 'PassRecover' ||
            to.name === 'ok_pass' ||
            to.name === 'error_pass'
        ) {
            window.scrollTo(0,0);
            next('/dashboard');
        } else {
            window.scrollTo(0,0);
            if (response.phone==null){
                next('/');
            }else {
                next();
            }
        }
    }).catch(function (error) {
        console.log(error)
        console.log(localStorage.firstTime)
        /*if (!localStorage.firstTime){
            next('/slides');
            localStorage.firstTime=1;
        } else { */
          /*  if (
                !(to.name === 'Ingresar') &&
                !(to.name === 'Registro') &&
                !(to.name === 'Recuperar-Pass') &&
                !(to.name === 'ok_pass') &&
                !(to.name === 'error_pass') &&
                !(to.name === 'Slider')
            ) {
                next('/pages/login');
                return;
            }
        //}
        throw error;
    });

    // sigue de largo
    window.scrollTo(0,0);
   next();
}); */





/*router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem('access_token') == null) {
      next({
        path: '/pages/login',
        params: { nextUrl: to.fullPath }
      })
    } else {
      let user = JSON.parse(localStorage.getItem('user'))
      if(to.matched.some(record => record.meta.is_admin)) {
        if(user.is_admin == 1){
          next()
        }
        else{
          next({ name: '/'})
        }
      }else {
        next()
      }
    }
  } else if(to.matched.some(record => record.meta.guest)) {
    if(localStorage.getItem('access_token') == null){
      next()
    }
    else{
      next({ name: '/'})
    }
  }else {
    next()
  }
})*/

export default router
