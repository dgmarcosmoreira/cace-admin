import Vue from "vue";
import Vuex from "vuex";

import ui from "./modules/ui";
import user from "./modules/user/index";
import course from "./modules/course/index";
import VuexPersistence from "vuex-persist";

Vue.use(Vuex);

const vuexLocal = new VuexPersistence({
  storage: window.localStorage
});

const store = {
  modules: {
    ui,
    user,
    course
  },
  plugins: [vuexLocal.plugin]
};

export default new Vuex.Store(store);
