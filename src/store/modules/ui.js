import API from '../../services';

// initial state
const state = {
  loading: false,
};

// actions
const actions = {
  toggleSpinner({ commit, state }) {
    commit('setLoading', !state.loading);
  }
};

// mutations
const mutations = {
  set_loading(state, loading) {
    state.loading = loading;
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
