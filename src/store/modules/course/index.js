import actions from "./actions";
import getters from "./getters";
import mutations from "./mutations";

export default {
  getters,
  actions,
  mutations,
  namespaced: true
};
