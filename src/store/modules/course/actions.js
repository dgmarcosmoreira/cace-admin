import router from "@/router";
import coursesService from "../../../services/course";

// identificar si es teatro o espectaculo
export default {
  async getCourses({ commit }, count) {
    try {
      const courses = await coursesService.getCourses();
      commit("setCourses", courses);
    } catch (error) {
      alert("error");
    }
  },
  async getCourse({ commit }, courseId) {
    try {
      const course = await coursesService.getCourse(courseId);
      commit("setCourse", course);
    } catch (error) {
      alert("error");
    }
  },
  async editCourse({ commit }, data) {
    try {
      await coursesService.editCourse();
    } catch (error) {
      alert("error");
    }
  }
};
