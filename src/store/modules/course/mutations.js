export default {
  set_courses(state, courses) {
    state.courses = courses;
  },
  set_course(state, course) {
    state.course = course;
  },
};
