import API from '../../../services';
import router from '@/router';

// identificar si es teatro o espectaculo
export default {

  /* async login({ commit }, data) {
    try {
      const res = await API.user.login(data);

      localStorage.setItem('access_token', res.data.access_token);

      commit('setUser', res.data.user);

      if (res.data.user.type === 1) {
        // es un espectaculo
        const profile = await API.event.getProfile();
        commit('event/setProfile', profile, { root: true });
        router.push('/espectaculo/propuestas');
      } else {
        if (res.data.user.type === 2) {
          const profile = await API.theatre.getProfile();
          commit('theatre/setProfile', profile, { root: true });
          router.push('/sala/cerca');
        } else {
          const profile = await API.theatre.getProfile();
          commit('theatre/setProfile', profile, { root: true });
          router.push('/sala/cerca');
        }
      }
    } catch (error) {
      console.log(error);
    }
  },

  async logout({ commit }) {
    try {
      await localStorage.clear();
      await API.user.logout();

      commit('setUser', null);
      commit('setProfile', null);
    } catch (error) {
      console.log(error);
    }
  },

  async changePassword({ commit }, password) {
    try {
      await API.user.postChangePassword(password);
      router.push('espectaculo/ok/contrasena-modificada');
    } catch (error) {
      console.log(error);
    }
  },

  async passwordRecovery({ commit }, email) {
    try {
      await API.user.postPasswordRecovery(email);
      Swal.fire({
        text:
          'Se envió un enlace de recuperación a tu E-Mail!',
        type: 'success',
        confirmButtonColor: '#f24021',
        confirmButtonText: 'Aceptar'
      });
      router.push('/');
    } catch (error) {
      console.log(error);
    }
  },

  async passwordReset({ commit }, data) {
    try {
      console.log(data);
      await API.user.postPasswordReset(data);
      Swal.fire({
        text:
          'Contraseña restablecida.',
        type: 'success',
        confirmButtonColor: '#f24021',
        confirmButtonText: 'Aceptar'
      });
      router.push('/');
    } catch (error) {
      console.log(error);
    }
  },

  async getResponsabilities({ commit }) {
    try {
      const responsabilities = await API.user.getResponsabilities();
      commit('setResponsabilities', responsabilities);
    } catch (error) {
      console.log(error);
    }
  },

  async registerEvent({ commit }, data) {
    try {
      const res = await API.user.registerEvent(data);
      if (res.user) {
        const response = await API.user.login(data.user);
        localStorage.setItem('access_token', response.data.access_token);
        commit('setUser', response.data.user);
        router.push('/espectaculo/home');
      }
    } catch (error) {
      console.log(error);
    }
  },

  async updateAccount({ commit, rootState }, data) {
    try {
      await API.user.updateAccount(data);
      router.push(
        rootState.user.user.type === 1
          ? `/espectaculo/ok/mi-cuenta`
          : `/sala/ok/mi-cuenta`
      );
    } catch (error) {
      console.log(error);
    }
  },*/


};
