import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
  user: JSON.parse(localStorage.getItem('user'))
};

export default {
  state,
  getters,
  actions,
  mutations,
  namespaced: true
};
