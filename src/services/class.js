import axios from "./axiosInstance";

const addClass = async data => {
  return axios
    .post("classes", data)
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};
/*const getClasses= async courseId => {
    return axios
        .get(`classes/${courseId}`)
        .then(({data}) => data)
        .catch(error => {
            throw error;
        });
};*/
const getClasses = async => {
  return axios
    .get("classes")
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};
const editClass = async (id, data) => {
  return axios
    .put(`classes/${id}`, data)
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};
const delClass = async id => {
  return axios
    .delete(`classes/${id}`)
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};
export default {
  getClasses,
  addClass,
  editClass,
  delClass
};
