import axios from "./axiosInstance";

const getFiles = async => {
  return axios
    .get("files")
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};
const getFile = async id => {
  return axios
    .get(`files/${id}`)
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};
const getGallery = async id => {
    return axios
        .get(`files/search?course_id=${id}&scope=image`)
        .then(({ data }) => data)
        .catch(error => {
            throw error;
        });
};
const getMaterial = async id => {
    return axios
        .get(`files/search?course_id=${id}&scope=file`)
        .then(({ data }) => data)
        .catch(error => {
            throw error;
        });
};
const getVideos = async id => {
    return axios
        .get(`files/search?course_id=${id}&scope=video`)
        .then(({ data }) => data)
        .catch(error => {
            throw error;
        });
};

const addFile = async (data, config) => {
  return axios
    .post("files", data, config)
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};3
const delFile = async id => {
  return axios
    .delete(`files/${id}`)
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};

export default {
  getFiles,
  getFile,
  addFile,
  delFile,
    getGallery,
    getMaterial,
    getVideos,
};
