import axios from "./axiosInstance";

const addUnit = async data => {
  return axios
    .post("units", data)
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};
const getUnit = async unitId => {
    return axios
        .get(`units/${unitId}`)
        .then(({data}) => data)
        .catch(error => {
            throw error;
        });
};
const getUnits = async => {
  return axios
    .get("units")
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};
const editUnit = async (id, data) => {
  return axios
    .put(`units/${id}`, data)
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};
const delUnit = async id => {
  return axios
    .delete(`units/${id}`)
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};
export default {
  getUnits,
    getUnit,
  addUnit,
  editUnit,
  delUnit
};
