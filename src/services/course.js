import axios from "./axiosInstance";

// eslint-disable-next-line no-unused-vars
const getCourses = async => {
    return axios
        .get("courses")
        .then(({data}) => data)
        .catch(error => {
            throw error;
        });
};
const getCourse = async courseId => {
    return axios
        .get(`courses/${courseId}`)
        .then(({data}) => data)
        .catch(error => {
            throw error;
        });
};
const addCourse = async data => {
    return axios
        .post('courses', data)
        .then(({data}) => data)
        .catch(error => {
            throw error;
        });
};
const delCourse = async id => {
    return axios
        .delete(`courses/${id}`)
        .then(({data}) => data)
        .catch(error => {
            throw error;
        });
};
const editCourse = async (id, data) => {
    return axios
        .put(`courses/${id}`, data)
        .then(({data}) => data)
        .catch(error => {
            throw error;
        });
};


export default {
    getCourses,
    getCourse,
    addCourse,
    delCourse,
    editCourse
};
