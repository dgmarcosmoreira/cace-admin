import axios from "./axiosInstance";

const getTeachers = async => {
  return axios
    .get("teachers")
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};
const getTeacher = async TeacherId => {
  return axios
    .get(`teachers/${TeacherId}`)
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};

const addTeacher = async data => {
  return axios
    .post("teachers", data)
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};
const delTeacher = async id => {
  return axios
    .delete(`teachers/${id}`)
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};

const editTeacher = async (id, data) => {
  return axios
    .put(`teachers/${id}`, data)
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};

export default {
  getTeachers,
  getTeacher,
  addTeacher,
  delTeacher,
  editTeacher
};
