import axios from "./axiosInstance";

const addMaterial = async data => {
  return axios
    .post("materials", data)
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};
/*const getMaterials= async courseId => {
    return axios
        .get(`materials/${courseId}`)
        .then(({data}) => data)
        .catch(error => {
            throw error;
        });
};*/
const getMaterials = async => {
  return axios
    .get("materials")
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};
const editMaterial = async (id, data) => {
  return axios
    .put(`materials/${id}`, data)
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};
const delMaterial = async id => {
  return axios
    .delete(`materials/${id}`)
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};
export default {
  getMaterials,
  addMaterial,
  editMaterial,
  delMaterial
};
