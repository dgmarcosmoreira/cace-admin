import axios from "./axiosInstance";

const getCategory = async categoryId => {
    return axios
        .get(`categories/${categoryId}`)
        .then(({data}) => data)
        .catch(error => {
            throw error;
        });
};
const getCategories = async => {
    return axios
        .get("categories")
        .then(({data}) => data)
        .catch(error => {
            throw error;
        });
};
const addCategory = async data => {
    return axios
        .post('categories', data)
        .then(({data}) => data)
        .catch(error => {
            throw error;
        });
};
const editCategory = async (id, data) => {
    return axios
        .put(`categories/${id}`, data)
        .then(({data}) => data)
        .catch(error => {
            throw error;
        });
};
const delCategory = async id => {
    return axios
        .delete(`categories/${id}`)
        .then(({ data }) => data)
        .catch(error => {
            throw error;
        });
};
export default {
    getCategory,
    getCategories,
    addCategory,
    editCategory,
    delCategory
};
