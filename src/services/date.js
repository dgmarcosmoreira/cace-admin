import axios from "./axiosInstance";

const addDate = async data => {
  return axios
    .post("dates", data)
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};
const getDates= async courseId => {
    return axios
        .get(`dates/course/${courseId}`)
        .then(({data}) => data)
        .catch(error => {
            throw error;
        });
};
/*const getDates = async => {
  return axios
    .get("dates")
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};*/
const editDate = async (id, data) => {
  return axios
    .put(`dates/${id}`, data)
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};
const delDate = async id => {
  return axios
    .delete(`dates/${id}`)
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};
export default {
  getDates,
  addDate,
  editDate,
  delDate
};
