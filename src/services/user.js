import axios from "./axiosInstance";

const register = async data => {
    return axios
        .post('register', data)
        .then(({ data }) => data)
        .catch(error => {
            throw error;
        });
};

const login = async data => {
    return axios
        .post('login', data)
        .then(({ data }) => data)
        .catch(error => {
            throw error;
        });
};

// eslint-disable-next-line no-unused-vars
const logout = async => {
    return axios
        .delete('logout')
        .then(({ data }) => data)
        .catch(error => {
            throw error;
        });
};

// eslint-disable-next-line no-unused-vars
const getMyProfile = async => {
    return axios
        .get('users/me')
        .then(({ data }) => data)
        .catch(error => {
            throw error;
        });
};

// eslint-disable-next-line no-unused-vars
const resetPassword = async email => {
    return axios
        .post('password/create', { email })
        .then(({ data }) => data)
        .catch(error => {
            throw error;
        });
};

const getUser = async userId => {
    return axios
        .get(`users/${userId}`)
        .then(({data}) => data)
        .catch(error => {
            throw error;
        });
};
// eslint-disable-next-line no-unused-vars
const getUserName = async userId => {
    return axios
        .get(`users/${userId}`)
        .then(({data}) => data.name)
        .catch(error => {
            throw error;
        });
};

// eslint-disable-next-line no-unused-vars
const getUsers = async => {
    return axios
        .get("users")
        .then(({data}) => data)
        .catch(error => {
            throw error;
        });
};

const getUsersByRole = async role => axios
    .get(`users?role=${role}`)
    .then(({ data }) => data)
    .catch((error) => {
        throw error;
    });

const addUser = async data => {
    return axios
        .post('users', data)
        .then(({data}) => data)
        .catch(error => {
            throw error;
        });
};
const editUser = async (id, data) => {
    return axios
        .put(`users/${id}`, data)
        .then(({data}) => data)
        .catch(error => {
            throw error;
        });
};
const delUser = async id => {
    return axios
        .delete(`users/${id}`)
        .then(({ data }) => data)
        .catch(error => {
            throw error;
        });
};
export default {
    register,
    login,
    logout,
    getMyProfile,
    getUser,
    getUsers,
    getUsersByRole,
    addUser,
    editUser,
    delUser
};
