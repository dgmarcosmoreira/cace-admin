import axios from './axiosInstance';

// eslint-disable-next-line no-unused-vars
const getPlaces = async => axios.get('places')
  .then(({ data }) => data)
  .catch((error) => {
    throw error;
  });
const getPlace = async placeId => axios.get(`places/${placeId}`)
  .then(({ data }) => data)
  .catch((error) => {
    throw error;
  });

const addPlace = async data => axios.post('places', data)
  // eslint-disable-next-line no-shadow
  .then(({ data }) => data).catch((error) => {
    throw error;
  });
const delPlace = async id => axios.delete(`places/${id}`)
  .then(({ data }) => data)
  .catch((error) => {
    throw error;
  });

const editPlace = async (id, data) => axios.put(`places/${id}`, data)
  // eslint-disable-next-line no-shadow
  .then(({ data }) => data).catch((error) => {
    throw error;
  });

export default {
  getPlaces,
  getPlace,
  addPlace,
  delPlace,
  editPlace,
};
