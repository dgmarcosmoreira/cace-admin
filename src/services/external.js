import axios from "axios";

const getCountries = async => {
    return axios
        .get("https://restcountries.eu/rest/v2")
        .then(({data}) => data)
        .catch(error => {
            throw error;
        });
};
export default {
    getCountries
};
