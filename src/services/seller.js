import axios from "./axiosInstance";
import user from "./user";

const getSellers = async id => {
    return axios
        .get(`sellers/course/${id}`)
        .then(({ data }) => data)
        .catch(error => {
            throw error;
        });
};

const getAllSellers = async => {
    return axios
        .get("sellers")
        .then(({data}) => data)
        .catch(error => {
            throw error;
        });
};

const getSeller = async SellerId => {
  return axios
    .get(`sellers/${SellerId}`)
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};

const addSeller = async data => {
  return axios
    .post("sellers", data)
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};
const delSeller = async id => {
  return axios
    .delete(`sellers/${id}`)
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};

const editSeller = async (id, data) => {
  return axios
    .put(`sellers/${id}`, data)
    .then(({ data }) => data)
    .catch(error => {
      throw error;
    });
};

export default {
  getSellers,
    getAllSellers,
  getSeller,
  addSeller,
  delSeller,
  editSeller
};
