import axios from "axios";
import Vue from "vue";
import router from "@/router/index";
import Toaster from "v-toaster";
Vue.use(Toaster, { timeout: 5000 });

const instance = axios.create({
  // this must be a ENV
  // baseURL: "http://localhost:8080",
    baseURL: "http://api.caceglobal.org/api/",
  withCredentials: false,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json"
  }
});

instance.interceptors.request.use(
    config => {
        config.headers.Authorization = `Bearer ${localStorage.getItem(
            'access_token'
        )} `;
        return config;
    },
    function(error) {
        return Promise.reject(error);
    }
);

instance.interceptors.response.use(function(response) {return response;},
    function(error) {
      const {
        response: { status, data }
      } = error;

      console.log(response)
      if (status === 401) {

        // localStorage.removeItem('access_token');
        if (data.messages === 'token_expired') {
          Vue.prototype.$toaster.error('El token expiró');
        } else {
          Vue.prototype.$toaster.error(data.messages);
        }
      } else {

        if (status === 500) {
          //localStorage.removeItem('access_token');
          Vue.prototype.$toaster.error('hubo problemas con el servidor');
          router.push('/');
        } else {
          if (status === 422) {
            Vue.prototype.$toaster.error(data);
            //router.push('/pages/login');
          }
          if (status === 400) {
            Vue.prototype.$toaster.error(data);
          } else {
            if (localStorage.getItem('access_token') !== null) {
              Vue.prototype.$toaster.error(error.response.data.messages);
              //router.push('/dashboard');
            }
          }
        }
      }

      throw error.response;
    }
);

export default instance;
