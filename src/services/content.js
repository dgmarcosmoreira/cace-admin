import axios from "./axiosInstance";

const getContents = async => {
    return axios
        .get("contents")
        .then(({data}) => data)
        .catch(error => {
            throw error;
        });
};
const getContent = async courseId => {
    return axios
        .get(`contents/${courseId}`)
        .then(({data}) => data)
        .catch(error => {
            throw error;
        });
};


const addContent = async data => {
    return axios
        .post('contents', data)
        .then(({data}) => data)
        .catch(error => {
            throw error;
        });
};
const delContent = async id => {
    return axios
        .delete(`contents/${id}`)
        .then(({data}) => data)
        .catch(error => {
            throw error;
        });
};

const editContent = async (id, data) => {
    return axios
        .put(`contents/${id}`, data)
        .then(({ data }) => data)
        .catch(error => {
            throw error;
        });
};


export default {
    getContents,
    getContent,
    addContent,
    delContent,
    editContent
};
