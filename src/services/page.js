import axios from "./axiosInstance";

const getPage = async userId => {
    return axios
        .get(`pages/${userId}`)
        .then(({data}) => data)
        .catch(error => {
            throw error;
        });
};
const getPages = async => {
    return axios
        .get("pages")
        .then(({data}) => data)
        .catch(error => {
            throw error;
        });
};
const addPage = async data => {
    return axios
        .post('pages', data)
        .then(({data}) => data)
        .catch(error => {
            throw error;
        });
};
const editPage = async (id, data) => {
    return axios
        .put(`pages/${id}`, data)
        .then(({data}) => data)
        .catch(error => {
            throw error;
        });
};
export default {
    getPage,
    getPages,
    addPage,
    editPage
};
