const items = [
    {
      name: "Home",
      url: "/dashboard",
      icon: "icon-speedometer",
    },
    {
      name: "Pedidos",
      url: "/orders",
      icon: ""
    },
    {
      name: "Inscripciones",
      url: "/inscriptions",
      icon: ""
    },
    {
      name: "Cursos",
      url: "/courses",
      children: [
        {
          name: "Todos los cursos",
          url: "/courses",
          icon: ""
        },
        {
          name: "Agregar curso",
          url: "/course/add",
          icon: ""
        },
        {
          name: "Categorías",
          url: "/categories",
          icon: ""
        }
      ]
    },
    {
      name: "Sedes",
      url: "/places",
      icon: ""
    },
    {
      name: "Docentes",
      url: "/teachers",
      icon: ""
    },
    {
      name: "Usuarios",
      url: "/users",
      icon: ""
    },
    {
      name: "Páginas",
      url: "/pages",
      icon: ""
    },
    {
      name: "Novedades",
      url: "/news",
      icon: ""
    },
  ]
export default items;





